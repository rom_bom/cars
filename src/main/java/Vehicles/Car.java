package Vehicles;

import Detailsi.Engine;
import Professions.Driver;

public class Car {
    private String brand;
    private String xclass;
    private double weight;
    private Driver driver;
    private Engine engine;

    public Car(String brand, String xclass, double weight, Driver driver, Engine engine, String brand1) {
        this.brand = brand;
        this.xclass = xclass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    public Car(String brand, String xclass, double weight, Driver driver, Engine engine) {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getXclass() {
        return xclass;
    }

    public void setXclass(String xclass) {
        this.xclass = xclass;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void start() {
        System.out.println("Поїхали");
    }

    public void stop() {
        System.out.println("Зупиняємося");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", xclass='" + xclass + '\'' +
                ", weight=" + weight +
                ", driver=" + driver +
                ", engine=" + engine +
                '}';
    }
}
