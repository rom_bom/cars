package Professions;

public class Person {
    private String fullName;
    private int age;
    private String region;

    public Person(String fullName) {
    }

    public void Person(String fullName) {
        int age;
        String region;
    }

    public Person() {
    }
    public Person(String fullName, int age, String region) {
        this.fullName = fullName;
        this.age = age;
        this.region = region;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Person{"
                + "fullName='" + fullName + '\''
                + ", age=" + age
                + ", region="
                + '}';
    }
}

